public class Ciudadano {
    // Definición de atributos
    private int id;
    private boolean infectado;
    private boolean estado;
    private int contador;
    private boolean inmune;
    private int sanos;

    // Constructor con parámetros
    Ciudadano(int id) {
	this.id = id;
	this.infectado = false;
	this.estado = false;
	this.contador = 0;
	this.inmune = false;
    }

    // Getter's

    public int getId() {

	return this.id;
    }

    public boolean getEstado() {

	return this.estado;
    }

    public int getContador() {

	return this.contador;
    }

    public boolean getInfectado() {

	return this.infectado;

    }

    public boolean getInmune() {

	return this.inmune;
    }

    public int getSanos() {

	return this.sanos;
    }

    // Setter's

    public void setId(int id) {

	this.id = id;
    }

    public void setInfectado(boolean infectado) {

	this.infectado = infectado;
    }

    public void setEstado(boolean estado) {

	this.estado = estado;
    }

    public void setContador(int contador) {
	this.contador = contador;
    }

    public void setInmune(boolean inmune) {
	this.inmune = inmune;
    }

    public void setSanos(int sanos) {

	this.sanos = sanos;
    }

}