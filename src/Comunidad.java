import java.util.ArrayList;
import java.util.Random;

public class Comunidad {

    // Definición de atributos
    private int nro_ciudadano;
    private ArrayList<Ciudadano> ciudadano = new ArrayList<Ciudadano>();
    private double promedio_conexion = 8;
    private Enfermedad enfermedad;
    private int numero_infectados = 10;
    private double probabilidad_conexion = 0.9;

    // Constructor por parametros
    public Comunidad(int nro_ciudadano, double promedio_conexion, Enfermedad enfermedad, int numero_infectados,
	    double probabilidad_conexion) {

	this.nro_ciudadano = nro_ciudadano;
	this.promedio_conexion = promedio_conexion;
	this.enfermedad = enfermedad;
	this.numero_infectados = numero_infectados;
	this.probabilidad_conexion = probabilidad_conexion;

	// Ciclo genera objetos y los inserta en una lista
	for (int i = 0; i < nro_ciudadano; i++) {
	    this.ciudadano.add(new Ciudadano(i + 1));

	}

	// Primeros 10 iniciales (infectados ALEATORIOS) del ArrayList "ciudadanos" y su
	// estado
	Random rand = new Random();
	int aux = 0;
	int seleccionados;

	while (aux < numero_infectados) {
	    seleccionados = rand.nextInt(this.ciudadano.size());
	    if (!this.ciudadano.get(seleccionados).getInfectado()) {
		this.ciudadano.get(seleccionados).setInfectado(true);
		aux++;

	    }

	}

    }

    // Getter's

    public int getNro_ciudadano() {

	return this.nro_ciudadano;
    }

    public ArrayList<Ciudadano> getCiudadano() {

	return this.ciudadano;
    }

    public int getNumeroInfectados() {

	return this.numero_infectados;
    }

    public double getPromedio_conexion() {

	return this.promedio_conexion;
    }

    public Enfermedad getEnfermedad() {

	return this.enfermedad;
    }

    public int getNumero_infectados() {

	return this.numero_infectados;
    }

    public double getProbabilidad_conexion() {

	return this.probabilidad_conexion;
    }

    // Setter's

    public void setNro_ciudadano(int nro_ciudadano) {

	this.nro_ciudadano = nro_ciudadano;
    }

    public void setProbabilidad_conexion(double probabilidad_conexion) {

	this.probabilidad_conexion = probabilidad_conexion;
    }

    public void setCiudadano(ArrayList<Ciudadano> ciudadano) {

	this.ciudadano = ciudadano;
    }

    public void setPromedio_conexion(double promedio_conexion) {
	this.promedio_conexion = promedio_conexion;
    }

    public void setNumero_infectados(int numero_infectados) {

	this.numero_infectados = numero_infectados;
    }

    public void setEnfermedad(Enfermedad enfermedad) {

	this.enfermedad = enfermedad;
    }

}