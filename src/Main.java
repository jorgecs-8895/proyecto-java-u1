
public class Main {
    // Definimos Main, como el inicializador de las clases y el código en general.

    public static void main(String[] args) {

	// Variables constructoras.
	Enfermedad enfermedad = new Enfermedad(0.3, 18);
	Comunidad comunidad = new Comunidad(1000, 8, enfermedad, 10, 0.9);
	Simulador simulador = new Simulador(comunidad, enfermedad);
	simulador.simular(40);

    }

}