
// Importación de librerias requeridas para el uso y generación del código
import java.util.Random;

public class Simulador {
    // Definición de atributos y objetos.
    private Comunidad comunidad;
    private Enfermedad enfermedad;
    private Ciudadano ciudadano;

    // Método Simulador
    public Simulador(Comunidad comunidad, Enfermedad enfermedad) {

	this.comunidad = comunidad;
	this.enfermedad = enfermedad;

    }

    // Simulación de días para la pandemia
    public void simular(int numero_dias) {

	for (int i = 0; i < numero_dias; i++) {
	    imprimir_contagiados(ciudadano);
	    contagio_contacto();

	}

    }

    public void contagio_contacto() {
	// Se genera valor aleatorio para la generación de probabilidad.
	Random rand = new Random();
	for (Ciudadano p : this.comunidad.getCiudadano()) {
	    if (!verificar_muerte(p)) {
		if (!verificar_inmune(p)) {
		    if (!p.getInfectado()) {
			// "Ecuación" para infectar a los ciudadanos
			if (rand.nextDouble() < (0.3 * 0.9)) {
			    p.setInfectado(true);
			}
		    } else {
			if (verificar_pasos(p, enfermedad.getPromPasos())) {
			    p.setInfectado(false);
			    p.setInmune(true);
			    p.setContador(0);
			} else {
			    // Establecer 10% de probabilidad de "muerte"
			    if (rand.nextDouble() < 0.9) {
				int contador = p.getContador();
				p.setContador(contador + 1);
			    } else {
				p.setEstado(true);
				p.setInfectado(false);
				p.setInmune(true);
				p.setContador(0);
			    }
			}
		    }
		}
	    }
	}

    }

    private boolean verificar_pasos(Ciudadano p, int promPasos) {

	int pasos = p.getContador();

	if (pasos == promPasos) {

	    return true;
	} else {

	    return false;
	}

    }

    // Chequeo del ciudadano para verificar si puede contagiarse o no.
    private boolean verificar_inmune(Ciudadano p) {

	if (p.getInmune()) {

	    return true;
	} else {

	    return false;
	}

    }

    private boolean verificar_muerte(Ciudadano p) {

	if (p.getEstado()) {

	    return true;
	} else {

	    return false;
	}
    }

    // Salida por pantalla del programa.
    public void imprimir_contagiados(Ciudadano p) {

	int cont_contagiado = 0;
	int cont_total = 0;

	for (Ciudadano c : comunidad.getCiudadano()) {
	    if (c.getInfectado()) {
		cont_contagiado++;
	    }
	    if (c.getInfectado() || c.getInmune() || c.getEstado()) {
		cont_total++;
	    }

	}

	System.out.println("El total de personas contagiadas de la Comunidad 1 son: " + cont_total + ", "
		+ "Casos Activos: " + cont_contagiado);
	System.out.println(" ");
    }

}
